let adminUser = localStorage.getItem("isAdmin");
let token = localStorage.getItem("token");
let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId');
let courseName;
if (adminUser == "false" || !adminUser) {
	alert("You don't have administrator rights to this page!")
	window.location.replace("./courses.html")
}
else {
	fetch(`https://radiant-bayou-23603.herokuapp.com/api/courses/${courseId}`)
	.then(res => res.json())
	.then(data => {
		courseName = data.name;
	})

		fetch(`https://radiant-bayou-23603.herokuapp.com/api/courses/${courseId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				courseId: courseId,
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if (data === true) {
				alert(`You have enabled the course ${courseName}!`)
				window.location.replace("./courses.html")
			}
			else {
				alert(`Something went wrong...`)
			}
		})
}