/*let adminUser = localStorage.getItem("isAdmin");
let cardFooter;
let modalButton = document.querySelector("#adminButton");

if(adminUser == "false" || !adminUser) {

	modalButton.innerHTML = null;

} else {

	modalButton.innerHTML =
	`
		<div class="col-md-2 offset-md-10">
			<a href="./addCourse.html" class="btn btn-block btn-dark">
				Add Course
			</a>
		</div>
	`

}

if (adminUser == "false" || !adminUser) {

fetch('http://localhost:4000/api/courses')
.then(res => res.json())
.then(data => {

	console.log(data);

	// Creates a variable that will store the data to be rendered
	let courseData;

	if(data.length < 1) {

		courseData = "No courses available"

	} else {

		courseData = data.map(course => {
			
			if(adminUser == "false" || !adminUser) {

				cardFooter = 
					`-
						<a href="./course.html?courseId=${course._id}" value="${course._id}" class="btn btn-dark text-white btn-block editButton">
							Select Course
						</a>
					`

			} else {	

				cardFooter =
					`
						<a href="./editCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-primary text-white btn-block editButton">
							Edit
						</a>
						<a href="./deleteCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-danger text-white btn-block deleteButton">
							Disable Course
						</a>
						<a href="./enrollees.html?courseId=${course._id}" value="${course._id}" class="btn btn-success text-white btn-block enrollees">
							Show Enrollees
						</a>
					`

			}

			return (
				`
					<div class="col-md-6 my-3">
						<div class="card">
							<div class="card-body">
								<h5 class="card-title">
									${course.name}
								</h5>
								<p class="card-text text-left">
									${course.description}
								</p>
								<p class="card-text text-right">
									₱ ${course.price}
								</p>
							</div>
							<div class="card-footer">
								${cardFooter}
							</div>
						</div>
					</div>
				`
			)

		// Replaces the comma's in the array with an empty string
		}).join("");

		

	}

	document.querySelector("#coursesContainer").innerHTML = courseData;

})

} else {

	fetch('http://localhost:4000/api/courses/allcourses')
	.then(res => res.json())
	.then(data => {

	console.log(data);

	// Creates a variable that will store the data to be rendered
	let courseData;

	if(data.length < 1) {

		courseData = "No courses available"

	} else {

		courseData = data.map(course => {
			
			if(adminUser == "false" || !adminUser) {

				cardFooter =
					`
						<a href="./course.html?courseId=${course._id}" value="${course._id}" class="btn btn-primary text-white btn-block editButton">
							Select Course
						</a>
					`

			} else {

				cardFooter =
					`
						<a href="./editCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-primary text-white btn-block editButton">
							Edit
						</a>
						<a href="./deleteCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-danger text-white btn-block deleteButton">
							Disable Course
						</a>
						<a href="./enable.html?courseId=${course._id}" value="${course._id}" class="btn btn-danger text-white btn-block deleteButton">
							Enable Course
						</a>
						<a href="./enrollees.html?courseId=${course._id}" value="${course._id}" class="btn btn-success text-white btn-block enrollees">
							Show Enrollees
						</a>
					`

			}

			return (
				`
					<div class="col-md-6 my-3">
						<div class="card">
							<div class="card-body">
								<h5 class="card-title">
									${course.name}
								</h5>
								<p class="card-text text-left">
									${course.description}
								</p>
								<p class="card-text text-right">
									₱ ${course.price}
								</p>
							</div>
							<div class="card-footer">
								${cardFooter}
							</div>
						</div>
					</div>
				`
			)

		// Replaces the comma's in the array with an empty string
		}).join("");

		

	}

	document.querySelector("#coursesContainer").innerHTML = courseData;

})
}

*/


let adminUser = localStorage.getItem("isAdmin");
let cardFooter;
let modalButton = document.querySelector("#adminButton");

if (adminUser == "false" || !adminUser) {

	modalButton.innerHTML = null;

} else {

	modalButton.innerHTML = 
	`
		<div class="col-md-2 offset-md-10">
			<a href="./addCourse.html" class="btn btn-block btn-secondary">
				Add Course
			</a>

		</div>

	`

}

if(adminUser == "true"){

	fetch('https://radiant-bayou-23603.herokuapp.com/api/courses/allcourses')
	.then(res => res.json())
	.then(data => {

		console.log(data)

		let courseData;

		if(data.length < 1){

			courseData = "No courses available"
		
		} else {
			courseData = data.map(course => {

				let active = course.isActive;
				console.log(active)

				if(course.isActive == "false" || !active) {
					cardFooter =
						`
							<a href="./editCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-secondary text-white btn-block editButton">
								Edit
							</a>

							<a href="./enable.html?courseId=${course._id}" value="${course._id}" class="btn btn-success text-white btn-block enableButton">
								Enable Course
							</a>
				
						`
				} else {
						cardFooter =
						`
							<a href="./editCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-secondary text-white btn-block editButton">
								Edit
							</a>

							<a href="./deleteCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-danger text-white btn-block deleteButton">
								Disable Course
							</a>

							<a href="./enrollees.html?courseId=${course._id}" value="${course._id}" class="btn btn-dark text-white btn-block viewButton">
								View Enrollees
							</a>			

				
						`

				}

					return (
						`
							<div class="col-md-6 my-3">
								<div class="card">
									<div class="card-body">
										<h5 class="card-title">
											${course.name}
										</h5>
										<p class="card-text text-left">
											${course.description}
										</p>
										<p class="card-text text-left">
											₱ ${course.price}
										</p>
									</div>
									<div class="card-footer">
										${cardFooter}
									</div>
								</div>
							</div>
						`
					)	
			}).join("");
		}

			document.querySelector("#coursesContainer").innerHTML = courseData;

	})

}


if(adminUser == "false" || !adminUser) {

		fetch('https://radiant-bayou-23603.herokuapp.com/api/courses')
		.then(res => res.json())
		.then(data => {

			console.log(data)

			let courseData;

			if(data.length < 1){
				courseData = "No courses available"				
			
			} else {
				courseData = data.map(course => {

					cardFooter =
					`
						<a href="./course.html?courseId=${course._id}" value="${course._id}" class="btn btn-secondary text-white btn-block editButton">
							Select Course
						</a>
					`					
					return (
						`
							<div class="col-md-6 my-3">
								<div class="card">
									<div class="card-body">
										<h5 class="card-title">
											${course.name}
										</h5>
										<p class="card-text text-left">
											${course.description}
										</p>
										<p class="card-text text-left">
											₱ ${course.price}
										</p>
									</div>
									<div class="card-footer">
										${cardFooter}
									</div>
								</div>
							</div>
						`
					)	
				}).join("");
			}

			document.querySelector("#coursesContainer").innerHTML = courseData;

		})

}